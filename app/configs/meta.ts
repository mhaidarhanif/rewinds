/**
 * Config Meta
 *
 * Meta data mostly for information and SEO purpose
 */

export const configMeta = {
  charset: 'utf-8',
  viewport: 'width=device-width,initial-scale=1',
  name: 'Rewinds',
  title: 'Rewinds – Remix Tailwind Starter Kit by @mhaidarhanif',
  description:
    'Rewinds is a Remix starter kit with Tailwind CSS family of libraries',
  newsletterName: 'Newsletter from M Haidar Hanif',
  url: 'https://rewinds.mhaidarhanif.com',
  color: '#ffffff',
  ogType: 'website',
  ogImageAlt: 'Rewinds – Remix Tailwind Starter Kit',
  ogImageType: 'image/png',
  ogImageUrl: 'assets/opengraph/rewinds-og.png',
  twiterImageUrl: 'assets/opengraph/rewinds-og.png',
  fbAppId: '',
  locale: 'en_US',
};
