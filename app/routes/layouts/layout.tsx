import { Layout } from '~/layouts';

export default function LayoutsLayoutRoute() {
  return (
    <Layout className="prose-config">
      <h1>Layout: Default</h1>
    </Layout>
  );
}
