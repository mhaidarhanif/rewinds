# Redirect routes

This "r" folder contains redirect routes, mostly to external links.

The intention is so have this simple URL format: https://oursite.com/r/name

To make it easier to type, then redirect instead of accessing directly.

Can also use simpler one like https://oursite.com/name by just copying the route files.
